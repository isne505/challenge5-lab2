#include <iostream>
#include <string>
#include "tree.h"
#include <vector>
#include <cstdlib>
#include <ctime>
#include <time.h>

using namespace std;

int main()
{
	Tree<int> mytree;
	vector<int>data;
	srand(time(NULL));
	int num;
	for (int i = 1; i <= 500; i++) 
	{
		num = rand() % 500;
		mytree.insert(num); //Insert 500 Random numbers.
		data.push_back(num);//push 500 Random numbers.
	}

	mytree.inorder();//sort the tree and show value
	int num2,num3;
	mytree.clear();//clear the tree
	mytree.balance(data, 0, data.size() - 1);  //call balance function
	cout << "Height = " << mytree.maxDepth() << endl;  //show maximum height
	cout << "Input Number to search: ";//input number do you want to seacrh
	cin >> num2;
	if (mytree.search(num2)) //call search function 
	{ 
		cout << "Found!!!" << endl;
	}
	else 
	{
		cout << "Don't Found!!!" << endl;
	}
	cout << "Input Number to delete: ";//input number do you want to delete
	cin >> num3;
	mytree.DeleteSearch(num3);  //call delete function to delete num3
	mytree.inorder();  //sort and show
	system("pause");
	return 0;
}
