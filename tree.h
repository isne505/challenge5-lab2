#include <queue>
#include <stack>
using namespace std;

#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node {
public:
	Node() { left = right = NULL; }
	Node(const T& el, Node *l = 0, Node *r = 0) {
		key = el; left = l; right = r;
	}
	T key;
	Node *left, *right;
};

template<class T>
class Tree {
public:
	Tree() { root = 0; }
	~Tree() { clear(); }
	
	void clear() { clear(root); root = 0; }
	bool isEmpty() { return root == 0; }
	void inorder() { inorder(root); }
	void insert(const T& el);
	void deleteNode(Node<T> *& node);
	void visit(Node<T> *p);  //function to show word
	void balance(vector<T> data, int first, int last);
	bool search(T num) { return search(root,num); }  //search function
	int maxDepth() { return maxDepth(root); };  //function to find max depth level
	void DeleteSearch(const T& var);  //function to delete search data
	
protected:
	Node<T> *root;
	void clear(Node<T> *p);
	void inorder(Node<T> *p);
	bool search(Node<T> *p,T num);
	int maxDepth(Node<T> *p);
};

template<class T>
void Tree<T>::clear(Node<T> *p)
{
	if (p != 0) 
	{
		clear(p->left);
		clear(p->right);
		delete p;
	}
}

template<class T>
void Tree<T>::inorder(Node<T> *p)
{
	if (p != 0)//loop until p equal zero
	{
		inorder(p->left);  //find the left most subtree
		visit(p);  //show word
		inorder(p->right);//find the right most subtree
	}
}

template<class T>
void Tree<T>::insert(const T &el) 
{
	Node<T> *p = root, *prev = 0;
	while (p != 0) 
	{
		prev = p;
		if (p->key < el)
			p = p->right;
		else
			p = p->left;
	}
	if (root == 0)
		root = new Node<T>(el);
	else if (prev->key<el)
		prev->right = new Node<T>(el);
	else
		prev->left = new Node<T>(el);
}

template<class T>
void Tree<T>::deleteNode(Node<T>*& node) 
{
    Node<T> *prev = 0, *tmp=node;
    if (node != 0) 
	{
        if(node->right == 0)
            node = node->left;
        else if(node->left == 0)
            node = node->right;
        else 
		{
            tmp = node->left;
            prev = node;
            while(tmp->right != 0) 
			{
                prev = tmp;
                tmp=tmp->right;
            }
            node->key = tmp->key;
            if(prev == node)
                prev->left = tmp->left;
            else prev->right = tmp->left;
        }
        delete tmp;
    }
}
template<class T>
void Tree<T>::visit(Node<T> *p)
{
	cout << p->key << endl;//show word
}
template<class T>
void Tree<T>::balance(vector<T> data, int first, int last) {//function to balance the tree
	if (first <= last)
	{ 
		int middle = (first + last) / 2;  //find the medium
		insert(data[middle]);  //store medium value
		balance(data, first, middle - 1);  //recursive from left
		balance(data, middle + 1, last);  //recursive from right
	}
}
template<class T>
bool Tree<T>::search(Node<T> *root,T num)//function that order word and display with function visit 
{
	if (root == NULL || root->key == num)
		return root;

	if (root->key < num)
		return search(root->right, num);  //recursive from right

	return search(root->left, num);  //recursive from left
}
template<class T>
int Tree<T>::maxDepth(Node<T> *p)  //function to find both depth of tree
{
	if (p == NULL)  //if node is empty
	{
		return 0;
	}
	else
	{
		int lDepth = maxDepth(p->left);	 //find left depth
		int rDepth = maxDepth(p->right);  //find right depth

		if (lDepth > rDepth)
			return(lDepth + 1);  //increase length by 1 if left depth more than right depth
		else return(rDepth + 1);  //increase length by 1 if right depth more than left depth
	}
}

template<class T>
void Tree<T>::DeleteSearch(const T &el) //function to delete search nodes 
{ 
	Node<T> *node = root, *prev = 0;
	while (node != 0)  //if node isn't empty
	{
		if (node->key == el)
			break;
		prev = node;
		if (node->key < el)  //if node less than insert value
			node = node->right;  //change to right node
		else node = node->left;  //change to left node
	}
	if (node != 0 && node->key == el)
		if (node == root)  //if node is equal to value
			deleteNode(root);  //call function delete to delete current node
		else if (prev->left == node)
			deleteNode(prev->left);  //call function delete to delete left node
		else
			deleteNode(prev->right);  //call function delete to delete right node
}

#endif
